FROM registry.gitlab.com/comiq/robotframework-container:latest

USER root

RUN pip install setuptools
RUN pip install wheel

RUN pip install requests \
  robotframework \
  robotframework-seleniumlibrary \
  robotframework-databaselibrary \
  mysql-connector-python-rf
